# This repository has moved.
This repository has moved to https://bitbucket.org/officialboomi/docker-images

No further updates will be made to this repository. It will be removed from BitBucket in the future.

