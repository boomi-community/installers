# Changelog

### release 3.1.1
- RUN-795 Update to Java 8u251
- RUN-683 Unable to set nonProxyHosts containing pipe | character in Docker container properties
- RUN-821 Remove support for docker installer script

### release 3.1.0
- Initial changelog tracking
- Kubernetes support version.
